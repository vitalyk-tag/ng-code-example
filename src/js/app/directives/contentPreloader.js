define(['./module', 'underscore'], function (directives, _) {
    'use strict';

    directives.directive('contentPreloader', ['$rootScope', '$q', function ($rootScope, $q) {
        return {
            restrict: 'A',
            link: function () {
                console.log('content preloader Directive');

                var bgSuccessHandler = function (element, url) {
                    angular.element(element).css({
                        'background-image': 'url("' + url + '")'
                    });
                };

                var imgSuccessHandler = function (element, src) {
                    angular.element(element).attr('src', src);
                };

                var loadImages = function (bgImages, urlAttr, successHandler) {
                    var deferred = $q.defer();
                    if (!bgImages.length) {
                        deferred.resolve()
                    } else {
                        var count = 0;
                        var countOne = function () {
                            count++;
                            if (count == bgImages.length) {
                                deferred.resolve()
                            }
                        };

                        _.each(bgImages, function (element, index) {
                            var src = angular.element(element).attr(urlAttr);

                            angular.element(new Image())
                                .bind('load', function () {
                                    successHandler(element, src);
                                    countOne();
                                })
                                .bind('error', function () {
                                    countOne();
                                })
                                .attr('src', src);
                        });
                    }
                    return deferred.promise
                };

                angular.element('#preloader-modal').addClass('active');

                var allLoaded = function () {
                    setTimeout(function () {
                        angular.element('#preloader-modal').removeClass('active');
                    }, 0)
                };

                setTimeout(function(){
                    var bgImages = angular.element('[data-image-bg]');
                    var imgImages = angular.element('[data-image-img]');

                    var wrap1 = loadImages(bgImages, 'data-image-bg', bgSuccessHandler);
                    var wrap2 = loadImages(imgImages, 'data-image-img', imgSuccessHandler);

                    $q.all([wrap1, wrap2]).then(allLoaded);
                }, 200);
            }
        }
    }]);
});