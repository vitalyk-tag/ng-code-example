/*globals define*/

define([
    './galleryHome',
    './animationSlidesHome',
    './scrollLinks',
    './slogans',
    './devices',
    './linksAnimation',
    './contactPopup',
    './servicesSlides',
    './appVsWeb',
    './team',
    './wrapperScroll',
    './mobileMenu',
    './popupsSurvey',
    './popupsSurveyAppVsWeb',
    './contentPreloader',
    './projectsNav',
    './projectsNavBottom',
    './parallaxNav'
], function () {
  'use strict';
});
