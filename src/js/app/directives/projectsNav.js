define(['./module'], function (directives) {
    directives.directive('projectsNav', [function(){
        return {
            restrict: 'A',
            template: '<div class="slider-nav"><a href="#portfolio/{{prevProject.route}}" class="prev" data-ng-if="prevProject">{{prevProject.name}}</a><a href="#portfolio/{{nextProject.route}}" class="next" data-ng-if="nextProject">{{nextProject.name}}</a></div>'
        }
    }])
});