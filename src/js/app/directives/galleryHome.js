/*globals define, console*/
define(['./module'], function (directives) {

    'use strict';
    directives.directive('galleryHome', function(){
        return {
            link: function (scope, element, attrs) {

                element.find(".gallery-control .gallery-control-block-connect").click(function(){
                    element.find(".gallery-control .gallery-control-block").removeClass("active");
                    element.find(".gallery-content>section").removeClass("active");
                    element.find(".gallery-content>section").removeClass("basic");
                    element.find(".gallery-content>section.gallery-section-connect").toggleClass("active");
                    angular.element(this).toggleClass("active");
                });

                element.find(".gallery-control .gallery-control-block-brainstorm").click(function(){
                    element.find(".gallery-control .gallery-control-block").removeClass("active");
                    element.find(".gallery-content>section").removeClass("active");
                    element.find(".gallery-content>section").removeClass("basic");
                    element.find(".gallery-content>section.gallery-section-brainstorm").toggleClass("active");
                    angular.element(this).toggleClass("active");
                });

                element.find(".gallery-control .gallery-control-block-develop").click(function(){
                    element.find(".gallery-control .gallery-control-block").removeClass("active");
                    element.find(".gallery-content>section").removeClass("active");
                    element.find(".gallery-content>section").removeClass("basic");
                    element.find(".gallery-content>section.gallery-section-develop").toggleClass("active");
                    angular.element(this).toggleClass("active");
                });

                element.find(".gallery-control .gallery-control-block-deliver").click(function(){
                    element.find(".gallery-control .gallery-control-block").removeClass("active");
                    element.find(".gallery-content>section").removeClass("active");
                    element.find(".gallery-content>section").removeClass("basic");
                    element.find(".gallery-content>section.gallery-section-deliver").toggleClass("active");
                    angular.element(this).toggleClass("active");
                });

                element.find(".gallery-control .gallery-control-block-launch").click(function(){
                    element.find(".gallery-control .gallery-control-block").removeClass("active");
                    element.find(".gallery-content>section").removeClass("active");
                    element.find(".gallery-content>section").removeClass("basic");
                    element.find(".gallery-content>section.gallery-section-launch").toggleClass("active");
                    angular.element(this).toggleClass("active");
                });

            }
        }

    })

});