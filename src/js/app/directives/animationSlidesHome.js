define(['./module', 'underscore'], function (directives, _) {

    'use strict';
    directives.directive('animationSlidesHome', ['$rootScope' ,function($rootScope){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                var h = $(window).height();

                $('#intro').parallax("50%", -1);
                $('#second').parallax("50%", -1);
                $('#third').parallax("50%", -1);

                var intro = $(".paralax-slider #intro");
                var second = $(".paralax-slider #second");
                var third = $(".paralax-slider #third");

                var sections = {intro: intro, second: second, third: third};

                var buttons = {};
                buttons.intro = $(".paralax-slider #nav .section-01 a");
                buttons.second = $(".paralax-slider #nav .section-02 a");
                buttons.third = $(".paralax-slider #nav .section-03 a");

                var clear = function(){
                    _.each(buttons, function(button){
                        button.removeClass('active');
                    });

                    _.each(sections, function(section){
                        section.removeClass('animation')
                    })
                };

                $(window).on('scroll.home',function(){
                    var offset = $(this).scrollTop();

                    _.each(sections, function(section, name){
                        if (offset >= section.offset().top - (section.height()*0.5)) {
                            clear();
                            section.addClass('animation');
                            buttons[name].addClass('active');
                        }
                    });
                });

                $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                    $(window).off('scroll.home');
                });

                function adjustBlocksHeightToWindowHeight() {

                    var finalBlockHeight;
                    if ($(window).height() > 940) {
                        finalBlockHeight = '940px !important';
                    } else {
                        finalBlockHeight = ($(window).height()) + 'px !important;'
                    }
                    //$('.project-table').attr('style', 'height: ' + finalBlockHeight);
                    $('.section-height').attr('style', 'height: ' + finalBlockHeight);
                }

                setTimeout(function() {
                    adjustBlocksHeightToWindowHeight();
                    jQuery(".slogan-slider").addClass("animation");
                },0);

                $(window).on('resize.home', function(){
                    adjustBlocksHeightToWindowHeight();
                });

                $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                    $(window).off('resize.home');
                });

            }
        }
    }])
});