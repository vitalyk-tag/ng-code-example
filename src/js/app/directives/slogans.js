define(['./module'], function(directives){
    'use strict';
    directives.directive('slogans', ['$rootScope', function($rootScope){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                scope.showShild = true;
                var testimonialsFlag = true,
                    testimonialsCurrent = 0,
                    testimonialsPause = 4500,

                    slideTestimonials = function(){
                        var testimonials = $(element).find('.testimonial');
                        console.log(testimonials);

                        if (testimonialsFlag && testimonials &&  testimonials.length > 1){
                            $(element).find('.testimonial').each(function(){
                                $(this).removeClass('opacity-09');
                            });

                            if (testimonialsCurrent === testimonials.length){
                                testimonialsCurrent = 0;
                            }
                            setTimeout(function(){
                                $(testimonials[testimonialsCurrent]).addClass('opacity-09');
                                testimonialsCurrent++;
                            }, 500);
                            setTimeout(slideTestimonials, testimonialsPause)
                        }
                    };

                slideTestimonials();

                $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                    testimonialsFlag = false;
                });

                //$(element).find('.bxslider-slogan').bxSlider();
                var player = $(element).find('#yt-home-page').YTPlayer({
                    fitToBackground: true,
                    videoId: '0xpoYs33N6s',
                    repeat: false,
                    playerVars: {
                        controls: 2,
                        autohide: 1,
                        loop: 1
                    },
                    callback: function(){
                        if (player && player.player){
                            player.player.addEventListener('onStateChange', function(data){
                                // playing of the video has finished
                                if (data.data === 0){
                                    scope.$apply(function(){
                                        scope.showShild = true;
                                        player.player.destroy()
                                    })
                                }
                                // video is playing
                                if (data.data === 1){
                                    scope.$apply(function(){
                                        scope.showShild = false;
                                    })
                                }
                            });
                        }
                    }
                }).data('ytPlayer');

                $(element).find('.bxslider-project').bxSlider();
                $(element).find('.bxslider-services').bxSlider();

                $(element).addClass("animation")
            }
        }
    }])
});
