define(['./module'], function (directives) {
    directives.directive('projectsNavBottom', [function(){
        var template = '<div class="slider-nav">';
        template +=         '<a class="arrow-up" href="#top-partition"></a>';
        template +=         '<a href="#/portfolio/{{prevProject.route}}" class="prev" data-ng-if="prevProject && prevProject.route">{{prevProject.name}}</span>';
        template +=         '<a href="#/portfolio/{{nextProject.route}}" class="next" data-ng-if="nextProject && nextProject.route">{{nextProject.name}}</span>';
        template +=         '<a class="table-ico" href="#top-partition"></a>';
        template +=     '</div>';

        return {
            restrict: 'A',
            template: template
        }
    }])
});