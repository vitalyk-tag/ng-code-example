define(['./module'], function (directives) {
    directives.directive('wrapperScroll', function () {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $(window).scroll(function () {
                    var wrapper = $('.wrapper');
                    if ($(this).scrollTop() > 1) {
                        wrapper.addClass("wrapper-scroll");
                    } else {
                        wrapper.removeClass("wrapper-scroll");
                    }
                });
            }
        }
    })
});