define(['./module'], function (directives) {

    'use strict';
    directives.directive('servicesSlides', function(){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                jQuery(".services-slider .services-slide-control .services-control.services-control-01").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-01").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-01").removeClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-01").addClass("active");
                });

                jQuery(".services-slider .services-slide-control  .services-control.services-control-02").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-02").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-02").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-02").removeClass("hide");
                });

                jQuery(".services-slider .services-slide-control  .services-control.services-control-03").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-03").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-03").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-03").removeClass("hide");
                });

                jQuery(".services-slider .services-slide-control  .services-control.services-control-04").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-04").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-04").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-04").removeClass("hide");
                });

                jQuery(".services-slider .services-slide-control  .services-control.services-control-05").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-05").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-05").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-05").removeClass("hide");
                });

                jQuery(".services-slider .services-slide-control  .services-control.services-control-06").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-06").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-06").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-06").removeClass("hide");
                });

                jQuery(".services-slider .services-slide-control  .services-control.services-control-07").click(function(){
                    jQuery(".services-slider .services-slide-control .services-control").removeClass("active");
                    jQuery(".services-slider .services-slide-control .services-control.services-control-07").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").removeClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide-07").addClass("active");
                    jQuery(".services-slider .bxslider-services .services-slide").addClass("hide");
                    jQuery(".services-slider .bxslider-services .services-slide-07").removeClass("hide");
                });

            }
        }

    })

});