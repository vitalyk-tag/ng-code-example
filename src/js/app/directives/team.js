define(['./module'], function (directives) {

    'use strict';
    directives.directive('team', function(){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $(element).find(".team-visual-control .control-block-01").click(function(){
                    $(element).find(".team-visual img").removeClass("active");
                    $(element).find(".team-visual .team-visual-img-01").addClass("active");
                    $(element).find(".team-visual-control .control-block").removeClass("active");
                    $(element).find(".team-visual-control .control-block-01").addClass("active");

                    $(element).find(".team-description .team-description-info").removeClass("active");
                    $(element).find(".team-description .team-description-info.person-01").addClass("active");
                });
                $(element).find(".team-visual-control .control-block-02").click(function(){
                    $(element).find(".team-visual img").removeClass("active");
                    $(element).find(".team-visual .team-visual-img-02").addClass("active");
                    $(element).find(".team-visual-control .control-block").removeClass("active");
                    $(element).find(".team-visual-control .control-block-02").addClass("active");

                    $(element).find(".team-description .team-description-info").removeClass("active");
                    $(element).find(".team-description .team-description-info.person-02").addClass("active");
                });
                $(element).find(".team-visual-control .control-block-03").click(function(){
                    $(element).find(".team-visual img").removeClass("active");
                    $(element).find(".team-visual .team-visual-img-03").addClass("active");
                    $(element).find(".team-visual-control .control-block").removeClass("active");
                    $(element).find(".team-visual-control .control-block-03").addClass("active");

                    $(element).find(".team-description .team-description-info").removeClass("active");
                    $(element).find(".team-description .team-description-info.person-03").addClass("active");
                });
                $(element).find(".team-visual-control .control-block-04").click(function(){
                    $(element).find(".team-visual img").removeClass("active");
                    $(element).find(".team-visual .team-visual-img-04").addClass("active");
                    $(element).find(".team-visual-control .control-block").removeClass("active");
                    $(element).find(".team-visual-control .control-block-04").addClass("active");

                    $(element).find(".team-description .team-description-info").removeClass("active");
                    $(element).find(".team-description .team-description-info.person-04").addClass("active");
                })
            }
        }
    })
});