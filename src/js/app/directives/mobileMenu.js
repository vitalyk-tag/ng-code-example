define(['./module'], function(directives){
    directives.directive('mobileMenu', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                var body = $("body"),
                    header = $("#header"),
                    nav = header.find("nav"),
                    logo = header.find(".logo"),
                    main = $(".main"),
                    wrapper = $(".wrapper");

                $(element).click(function(){
                    nav.toggleClass("opener");
                    logo.toggleClass("opener");
                    $(this).toggleClass("opener");
                    main.toggleClass("opener");
                    wrapper.toggleClass("opener");
                    wrapper.addClass("scroll-opener");
                    body.toggleClass("body-gray");
                });
                var closeHandler = function(){
                    nav.removeClass("opener");
                    logo.removeClass("opener");
                    header.find(".navigation_opener").removeClass("opener");
                    main.removeClass("opener");
                    wrapper.removeClass("opener");
                    body.removeClass("body-gray");
                    if (window.matchMedia('(min-width: 1000px)').matches) {
                        wrapper.removeClass("scroll-opener");
                    }
                };
                main.click(closeHandler);

                $('.navigation a').click(function(){
                    if (!$(this).hasClass('animation')){
                        closeHandler()
                    }
                });
            }
        }
    })
});