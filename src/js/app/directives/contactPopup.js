define(['./module'], function (directives) {
    directives.directive('contactPopup', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $(element).click(function () {
                    $("body").addClass("contact-popup-visible");
                });
                $(".popup-close").click(function () {
                    $("body").removeClass("contact-popup-visible");
                });

                $(".contact-popup .map-zoom").click(function () {
                    $(".contact-popup .contact-box").toggleClass("hide");
                    $(".contact-popup .contact-map").toggleClass("zoom");
                    $(".contact-popup header").toggleClass("zoom");
                });

                $('.contact-box input')
                    .focus(function () {
                        var placeholder = this.placeholder;
                        $(this).data('placeholder', placeholder);
                        this.placeholder = '';
                    })
                    .blur(function(){
                        this.placeholder = $(this).data('placeholder')
                    });

                $(".contact-box form").submit(function (event) {
                    event.preventDefault();
                    var formData = {
                        name: $('.contact-box input.name').val(),
                        website: $('.contact-box input.website').val(),
                        tel: $('.contact-box input.tel').val(),
                        email: $('.contact-box input.email').val()
                    };

                    $.ajax({
                        url: '/send',
                        type: 'POST',
                        data: formData,
                        encode: true,
                        dataType: 'json'
                    }).done(function (data) {
                        console.log('mail sent', arguments);
                        if (data.mes === 'ok') alert('Message is sent');
                    }).fail(function (xhr, stat, error) {
                        alert('Sending error, try again later');
                    });
                });
            }
        }
    })
});