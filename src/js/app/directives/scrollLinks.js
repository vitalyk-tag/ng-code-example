define(['./module'], function (directives) {

    'use strict';
    directives.directive('scrollLinks', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                setTimeout(function(){
                    $('a[href^="#"]').click(function() {
                        var target = $(this).attr('href');
                        if (target.indexOf('/') === -1){
                            var targetElement = $(target);
                            if (targetElement[0]){
                                $('html, body').animate({scrollTop: targetElement.offset().top}, 500);
                                return false;
                            }
                        }
                    });
                }, 10);

            }
        }

    })

});