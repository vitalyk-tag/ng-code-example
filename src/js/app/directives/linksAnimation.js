define(['./module'], function(directives){
    'use strict';
    directives.directive('linksAnimation', ['$templateCache', function($templateCache){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                setTimeout(function() {
                    $(element).find("ul.links li:eq(0)").addClass("animation");
                },1000);
                setTimeout(function() {
                    $(element).find("ul.links li:eq(1)").addClass("animation");
                },1500);
                setTimeout(function() {
                    $(element).find("ul.links li:eq(2)").addClass("animation");
                },2000);
                setTimeout(function() {
                    $(element).find("ul.links li").addClass("animation");
                },2000);
            }
        }
    }])
});