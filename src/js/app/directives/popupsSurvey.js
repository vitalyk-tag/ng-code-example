define(['./module', 'underscore'], function (directives, _) {
    directives.directive('popupsSurvey', ['$rootScope', '$compile', '$templateCache', '$sce', function($rootScope, $compile, $templateCache, $sce){
        return {
            restrict: 'A',
            template: $templateCache.get('survey_question')
        }
    }])
});