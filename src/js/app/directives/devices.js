define(['./module'], function(directives){
    'use strict';
    directives.directive('devices', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){

                if ( $(element).hasClass("tablet") ) {
                    $(element).find('head .viewport').attr('content', 'width=1200');
                    $(element).find('head .MobileOptimized').attr('content', 'width=1200');
                }
                if ( $(element).hasClass("desktop") ) {
                    $(element).find('head .viewport').attr('content', 'width=1200');
                    $(element).find('head .MobileOptimized').attr('content', 'width=1200');
                }
                if ( $(element).hasClass("mobile") ) {
                    $(element).find('head .viewport').attr('content', 'width=660');
                    $(element).find('head .MobileOptimized').attr('content', 'width=660');
                }
            }
        }
    })
});