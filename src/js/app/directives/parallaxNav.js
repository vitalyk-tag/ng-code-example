define(['./module'], function (directives) {

    'use strict';
    directives.directive('parallaxNav', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                setTimeout(function(){
                    jQuery(".paralax-slider #nav li a").each(function(){
                        $(this).bind('click', function(){
                            $(".paralax-slider #nav li a").removeClass('active');
                            $(this).addClass('active');
                        })
                    })
                }, 10);

            }
        }

    })

});
