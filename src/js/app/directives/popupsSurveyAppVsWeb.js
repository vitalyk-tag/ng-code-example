define(['./module', 'underscore'], function (directives, _) {
    directives.directive('popupsSurveyAppVsWeb', ['$rootScope', '$compile', '$templateCache', '$sce', function($rootScope, $compile, $templateCache, $sce){
        return {
            restrict: 'A',
            template: $templateCache.get('survey_question_app_vs_web')
        }
    }])
});