define(function(require){
    return [
        {route: 'project1', name: 'Rabadaba ApP', rank: 0, template: require('text!./project1.html')},
        {route: 'project2', name: 'Sportxast app', rank: 1, template: require('text!./project2.html')},
        {route: 'project3', name: 'Wisertogether system', rank: 2, template: require('text!./project3.html')}
    ];
});