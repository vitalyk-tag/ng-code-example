define(['angular', 'underscore','./index'],
    function(angular, _, projects){
        var module = angular.module('portfolio.projects.templates', []);

        module.run(['$templateCache', 'portfolioService',
            function($templateCache, portfolioService){
                portfolioService.projects = projects;

                _.each(projects, function(project){
                    $templateCache.put(project.route, project.template);
                })
            }
        ]);

        return module;
    });