/*globals define*/
define(function (require) {
    'use strict';
    var home = require('text!./home.html'),
        footer = require('text!./footer.html'),
        header = require('text!./header.html'),
        contact_popup = require('text!./contact-popup.html'),
        services = require('text!./services.html'),
        app_vs_web = require('text!./app-vs-web.html'),
        how_much = require('text!./how-much.html'),
        portfolio = require('text!./portfolio.html'),
        team = require('text!./team.html'),
        survey_question = require('text!./survey-question.html'),
        survey_summary = require('text!./survey-summary.html'),
        survey_question_app_vs_web = require('text!./survey-question-app-vs-web.html'),
        survey_summary_app_vs_web = require('text!./survey-summary-app-vs-web.html'),
        projects_index = require('text!./projects-index.html');
    return {
        'home': home,
        'footer': footer,
        'header': header,
        'contact_popup': contact_popup,
        'services': services,
        'app_vs_web': app_vs_web,
        'how_much': how_much,
        'portfolio': portfolio,
        'team': team,
        'survey_question': survey_question,
        'survey_summary': survey_summary,
        'survey_question_app_vs_web': survey_question_app_vs_web,
        'survey_summary_app_vs_web': survey_summary_app_vs_web,
        'projects_index': projects_index
    };
});