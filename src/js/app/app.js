/*globals define*/

define([
    'angular',
    'app/templates/main',
    'require',
    'angularroute',
    'app/controllers/index',
    'app/directives/index',
    'underscore',
    'app/services/index',
    'app/templates/projects/main'
], function (angular, templates, require) {
    'use strict';
    var app = angular.module('app', [
        'app.controllers',
        'app.directives',
        'app.services',
        'ngRoute',
        'seo',
        'portfolio.projects.templates'
    ]);

    app.templates = templates;

    app.run(['$templateCache', '$rootScope',
        function ($templateCache, $rootScope) {
            $templateCache.put('header', templates.header);
            $templateCache.put('footer', templates.footer);
            $templateCache.put('contact-popup', templates.contact_popup);
            $templateCache.put('survey_question', templates.survey_question);
            $templateCache.put('survey_summary', templates.survey_summary);
            $templateCache.put('survey_question_app_vs_web', templates.survey_question_app_vs_web);
            $templateCache.put('survey_summary_app_vs_web', templates.survey_summary_app_vs_web);
            $templateCache.put('projects_index', templates.projects_index);
        }
    ]);

    return app;
});