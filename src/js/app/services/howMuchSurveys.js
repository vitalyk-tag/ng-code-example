define(['./module'], function (services) {
    services.factory('howMuchService', function () {
        var factory = {},
            steps = {},
            summarys = 'step1';

        steps.step2 = {
            title: 'What type of app <br class="mobile-show"> are you building?',
            description: 'Most mobile apps require a<br class="mobile-show"> budget greater than 10k USD<br class="mobile-show"> while certain no-frills web<br class="mobile-show"> applications can be built for less.',
            step: 'step3',
            answers: [{
                icon: 'icon-android',
                title: 'Android',
                cost: 8000
            },{
                icon: 'icon-apple',
                title: 'apple ios',
                cost: 8000
            },{
                icon: 'icon-both',
                title: 'both',
                cost: 8000
            }]
        };

        steps.step3 = {
            title: 'Do people have to login?',
            description: 'An email login is generally best <br class="mobile-show"> to start with unless your app will <br class="mobile-show"> have tight integration with services <br class="mobile-show"> like Facebook or Twitter, in which case <br class="mobile-show"> social login is better.',
            step: 'step4',
            answers: [{
                icon: 'icon-email',
                title: 'email',
                cost: 2000
            },{
                icon: 'icon-social',
                title: 'social',
                cost: 2000
            },{
                icon: 'icon-no',
                title: 'no',
                cost: 0
            },{
                icon: 'icon-idk',
                title: 'I don’t know',
                cost: 0
            }]
        };

        steps.step4 = {
            title: 'Do people create personal profiles?',
            description: 'A profile means that some of the people <br class="mobile-show">  that use your app will have to enter <br class="mobile-show">  information about themselves <br class="mobile-show">  that might be viewed publicly.',
            step: 'step5',
            answers:[{
                icon: 'icon-yes',
                title: 'yes',
                cost: 2000
            },{
                icon: 'icon-no',
                title: 'no',
                cost: 0
            },{
                icon: 'icon-idk',
                title: 'I don’t know',
                cost: 0
            }]
        };

        steps.step5 = {
            title: 'How will you make money from your app?',
            description: 'Upfront cost is cheaper to build <br class="mobile-show"> than in-app purchases but in-app purchase <br class="mobile-show"> can produce higher returns <br class="mobile-show"> if/when you have an engaged user base.',
            step: 'step6',
            answers:[{
                icon: 'icon-cost',
                title: 'Upfront Cost',
                cost: 1000
            },{
                icon: 'icon-inapp',
                title: 'In-app Purchases',
                cost: 2000
            },{
                icon: 'icon-free',
                title: 'Free',
                cost: 0
            },{
                icon: 'icon-idk_cost',
                title: 'I don’t know',
                cost: 0
            }]
        };

        steps.step6 = {
            title: 'Do people rate or review things?',
            description: 'Keep in mind, ranking systems can vary <br class="mobile-show"> in complexity and thus impact your budget.',
            step: 'step7',
            answers:[{
                icon: 'icon-review',
                title: 'yes',
                cost: 3000
            },{
                icon: 'icon-no_review',
                title: 'no',
                cost: 0
            },{
                icon: 'icon-idk_review',
                title: 'I don’t know',
                cost: 0
            }]
        };

        steps.step7 = {
            title: 'Does your app need to connect <br class="mobile-show"> with your website?',
            description: 'This means you\'ll need to make an API <br class="mobile-show"> (or Application Programming Interface.) <br class="mobile-show"> It\'s how all your friendly <br class="mobile-show"> apps talk to each other.',
            step: 'step8',
            answers:[{
                icon: 'icon-connected',
                title: 'yes',
                cost: 5000
            },{
                icon: 'icon-not_connected',
                title: 'no',
                cost: 0
            },{
                icon: 'icon-idk_connected',
                title: 'I don’t know',
                cost: 0
            }]
        };

        steps.step8 = {
            title: 'How nice should your app look?',
            description: 'Custom gestures, custom buttons, and custom <br class="mobile-show"> transitions will cost you — it\'s not <br class="mobile-show"> cheap being pretty.',
            step: 'step9',
            answers:[{
                icon: 'icon-barebones',
                title: 'Bare bones',
                cost: 1900
            },{
                icon: 'icon-stock',
                title: 'Stock',
                cost: 4000
            },{
                icon: 'icon-beautiful',
                title: 'Beautiful',
                cost: 10200
            }]
        };

        steps.step9 = {
            title: 'Do you need an app icon?',
            description: 'Designing a quality app icon typically <br class="mobile-show"> ranges from $500 - $2,000 but will <br class="mobile-show"> help you stand out in the app store and on the device.',
            step: 'step1',
            answers:[{
                icon: 'icon-icon',
                title: 'yes',
                cost: 3200
            },{
                icon: 'icon-no_icon',
                title: 'no',
                cost: 0
            },{
                icon: 'icon-idk_icon',
                title: 'I don’t know',
                cost: 0
            }]
        };


        factory.steps = steps;
        factory.summarys = summarys;

        return factory;
    })
});