define(['./module'], function (services) {
    services.factory('appVsWebService', function () {
        var factory = {},
            steps = {},
            summarys = {};

        steps.step1 = {
            title: 'Is your budget<br class="mobile-show"> over 10 000$ ?',
            description: 'Most mobile apps require a<br class="mobile-show"> budget greater than 10k USD<br class="mobile-show"> while certain no-frills web<br class="mobile-show"> applications can be built for less.',
            step: 'step2',
            answers: [{
                icon: 'icon-no',
                title: 'no',
                step: 'step11'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        steps.step2 = {
            title: 'Do you need <br class="mobile-show">  location-based <br class="mobile-show">  or <br> navigational elements?',
            description: '',
            step: 'step3',
            answers: [{
                icon: 'icon-no',
                title: 'no'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        steps.step3 = {
            title: 'Do you need to <br class="mobile-show"> deliver information in <br> real-time?',
            description: 'Is there traffic if I turn <br class="mobile-show"> right at the next street?',
            step: 'step4',
            answers: [{
                icon: 'icon-no',
                title: 'no'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        steps.step4 = {
            title: 'Need access to features <br class="mobile-show"> native to your <br> phone like <br class="mobile-show"> Notifications or sensor info?',
            description: 'For example, Moves app <br class="mobile-show"> automatically tracks any <br class="mobile-show"> walking, cycling, and <br class="mobile-show"> running you do using sensor info from your iPhone.',
            step: 'step5',
            answers: [{
                icon: 'icon-no',
                title: 'no'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        steps.step5 = {
            title: 'Is your budget over $25k?',
            description: 'Mo\' money, fewer problems.',
            step: 'step6',
            answers: [{
                icon: 'icon-no',
                title: 'no'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        steps.step6 = {
            title: 'Do you want your design to be at a level <br> that’s ready to win a market?',
            description: '',
            step: 'step7',
            answers: [{
                icon: 'icon-no',
                title: 'no'
            },{
                icon: 'icon-yes',
                title: 'yes',
                step: 'step8'
            }]
        };

        steps.step7 = {
            title: 'Do you need it to work <br class="mobile-show"> well across many devices?',
            description: 'i.e. iPhones, iPads...',
            step: 'step8',
            answers: [{
                icon: 'icon-no',
                title: 'no',
                step: 'step10'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        steps.step8 = {
            title: 'Is your budget over $50-100k+?',
            description: '',
            step: 'step9',
            answers: [{
                icon: 'icon-no',
                title: 'no',
                step: 'step7'
            },{
                icon: 'icon-yes',
                title: 'yes'
            }]
        };

        summarys.step9 = {
            title: 'You should build...both',
            description: '',
            icons: ['vector-iphone-6', 'imac-flat']
        };

        summarys.step10 = {
            title: 'You should build...an app',
            description: '',
            icons: ['vector-iphone-6']
        };

        summarys.step11 = {
            title: 'You should build...A website',
            description: '',
            icons: ['imac-flat']
        };

        factory.steps = steps;
        factory.summarys = summarys;

        return factory;
    })
});