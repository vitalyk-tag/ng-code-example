/*globals define, console*/
define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('services_controller', ['$scope',
        function ($scope, $route) {
            $scope.htmlReady();
        }
    ]);
});