/*globals define*/

define([
    './partition_class_controller',
    './home_controller',
    './services_controller',
    './app_vs_web_controller',
    './how_much_controller',
    './portfolio_controller',
    './team_controller',
    './app_navigation'
], function () {
    'use strict';
});