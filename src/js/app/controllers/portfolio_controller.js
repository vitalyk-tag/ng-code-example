/*globals define, console*/
define(['./module', 'underscore', 'jquery'], function (controllers, _, $) {

    'use strict';
    controllers.controller('portfolio_controller', ['$scope', '$routeParams', 'portfolioService',
        function ($scope, $routeParams, portfolioService) {
            var projects = portfolioService.projects;

            $scope.project = $routeParams.project || 'projects_index';
            $scope.wrapper_class = 'full-wrapper';

            if ($routeParams.project !== 'projects_index'){
                var empty = {route: '', name: '', rank: ''};
                var currentProject = _.findWhere(projects, {route: $routeParams.project});

                if (currentProject){
                    $scope.prevProject = currentProject.rank - 1 < 0
                        ? empty
                        : _.findWhere(projects, {rank: currentProject.rank - 1});

                    $scope.nextProject = currentProject.rank + 1  >  projects.length - 1
                        ? empty
                        : _.findWhere(projects, {rank: currentProject.rank + 1});
                }
            }
            $(document).scrollTop(0);


            $scope.htmlReady();
        }
    ]);
});