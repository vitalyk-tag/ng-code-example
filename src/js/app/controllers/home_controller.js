/*globals define, console*/
define(['./module'], function (controllers) {

    'use strict';
    controllers.controller('home_controller', ['$scope',
        function ($scope) {
            $scope.htmlReady();
        }
    ]);
});