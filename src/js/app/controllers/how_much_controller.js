/*globals define, console*/
define(['./module', 'underscore'], function (controllers, _) {

    'use strict';
    controllers.controller('how_much_controller', ['$scope', '$sce', 'howMuchService',
        function ($scope, $sce, howMuchService) {
            var surveys = howMuchService.steps;

            var surveySummaryPage = howMuchService.summarys;

            $scope.total = 0;
            $scope.survey = {
                title: '',
                description: '',
                answers: []
            };

            var goNext = function(surveyName){
                var survey = surveys[surveyName];
                if (!survey) return;
                $scope.survey.name = surveyName;
                $scope.survey.title = $sce.trustAsHtml(survey.title);
                $scope.survey.description = $sce.trustAsHtml(survey.description);
                $scope.survey.answers = _.map(survey.answers, function(answer, index){
                    return {
                        icon: answer.icon,
                        index: index,
                        title: $sce.trustAsHtml(answer.title),
                        cost: answer.cost,
                        step: answer.step || survey.step
                    }
                });
            };

            $scope.surveyOpened = false;
            $scope.selectedAnswers = [];

            $scope.goHandler = function(nextSurvey, currentSurvey, answerIndex){
                if (surveys[currentSurvey]){

                    var currentAnswer = _.clone($scope.survey);
                    currentAnswer.answer = _.clone(currentAnswer.answers[answerIndex]);
                    delete currentAnswer.answers;
                    var cost = currentAnswer.answer.cost || 0;

                    var cachedAnswer = _.find($scope.selectedAnswers, function(answer){
                        return answer.name === currentSurvey;
                    });
                    var lastPick = 0;

                    if (cachedAnswer){
                        lastPick = cachedAnswer.answer.cost;
                        $scope.total += cost * 1 - lastPick;
                        cachedAnswer.answer.cost = cost;
                        cachedAnswer.answer.icon = currentAnswer.answer.icon
                    }else{
                        $scope.total += cost * 1 - lastPick;
                        $scope.selectedAnswers.push(currentAnswer);
                    }
                }
                nextSurvey = $scope.changeStep || nextSurvey;
                goNext(nextSurvey);
                $scope.surveyOpened = nextSurvey == surveySummaryPage ? 'summary' :'question';
                $scope.changeStep = currentSurvey == surveySummaryPage ? surveySummaryPage : false;
            };

            $scope.surveyClose = function(){
                $scope.surveyOpened = false;
            };

            $scope.openContactPopup = function(){
                $("body").addClass("contact-popup-visible");
            };
        }
    ]);
});