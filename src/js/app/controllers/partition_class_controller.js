/*globals define, console*/
define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('partition_class_controller', ['$scope', '$rootScope',
        function ($scope, $rootScope) {

            $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                var bodyClass = next && next.originalPath ? next.originalPath.substring(1): 'home';
                bodyClass = bodyClass || 'home';
                $scope.partition = bodyClass;
                $scope.htmlReady();
            });
        }
    ]);
});