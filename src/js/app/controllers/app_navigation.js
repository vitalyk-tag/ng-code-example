/*globals define, console*/
define(['./module', 'underscore'], function (controllers, _) {

    'use strict';
    controllers.controller('app.navigation', ['$scope', '$rootScope', '$location',
        function ($scope, $rootScope, $location) {

            //console.log('app.navigation', $rootScope);

            var buttons = $scope.buttons = {
                'services': false,
                'app_vs_web': false,
                'how_much': false,
                'portfolio': false,
                'team': false
            };
            var setActiveItem = function(itemName){
                itemName = itemName.split('-').join('_');
                _.each(buttons, function(val, name){
                    buttons[name] = false;
                });
                buttons[itemName] = true
            };

            var startPath = $location.path();
            startPath = startPath.split('/');
            startPath = startPath[1] ? startPath[1] : 'home';
            setActiveItem(startPath);

            var unbindRouteChangeSuccessHandler = $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                var bodyClass = next && next.originalPath ? next.originalPath.substring(1): 'home';
                bodyClass = bodyClass || 'home';
                setActiveItem(bodyClass);
                unbindRouteChangeSuccessHandler();
            });

            //$scope.htmlReady();
        }
    ]);
});