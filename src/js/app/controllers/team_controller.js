/*globals define, console*/
define(['./module'], function (controllers) {

    'use strict';
    controllers.controller('team_controller', ['$scope',
        function ($scope) {
            $scope.hideFotterTerms = true;
            $scope.hideFooterDesclimer = true;
            $scope.hideFooterCities = true;
            $scope.htmlReady();
        }
    ]);
});