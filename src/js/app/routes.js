/*globals define*/
/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(function (require) {
    'use strict';
    var main = require('./app'),
        routes = main.config(
            ['$routeProvider', '$locationProvider', '$httpProvider',
                function ($routeProvider, $locationProvider, $httpProvider) {
                    /*
                    $locationProvider.html5Mode({
                        enabled: true,
                        requireBase: false
                    });
                    */
                    $routeProvider
                        .when('/', {
                            template: main.templates.home,
                            controller: 'home_controller'
                        })
                        .when('/sample', {
                            template: main.templates.sample,
                            controller: 's_controller'
                        })
                        .when('/home', {
                            template: main.templates.home,
                            controller: 'home_controller'
                        })
                        .when('/services', {
                            template: main.templates.services,
                            controller: 'services_controller'
                        })
                        .when('/app-vs-web',{
                            template: main.templates.app_vs_web,
                            controller: 'app_vs_web_controller'
                        })
                        .when('/how-much',{
                            template: main.templates.how_much,
                            controller: 'how_much_controller'
                        })
                        .when('/portfolio',{
                            template: main.templates.portfolio,
                            controller: 'portfolio_controller'
                        })
                        .when('/portfolio/:project',{
                            template: main.templates.portfolio,
                            controller: 'portfolio_controller'
                        })
                        .when('/team',{
                            template: main.templates.team,
                            controller: 'team_controller'
                        })
                        .otherwise({
                            redirectTo: '/'
                        });
                }
            ]
        );
    return routes;
});
