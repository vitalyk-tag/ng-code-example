/*globals require,define*/
/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */
'use strict';

require.config({
    packages: [{
        name: 'app',
        location: '../js/app'
    }],
    paths: {
        'angular': '../bower_components/angular/angular',
        'angularroute': '../bower_components/angular-route/angular-route',
        'domReady': '../bower_components/requirejs-domready/domReady',
        'text': '../bower_components/requirejs-text/text',
        'jquery': '../bower_components/jquery/dist/jquery',
        'underscore': '../bower_components/underscore/underscore',
        'device': 'static/device',
        'parallax': 'static/jquery.parallax-1.1.3',
        'localscroll': 'static/jquery.localscroll-1.2.7-min',
        'scrollTo': 'static/jquery.scrollTo-1.4.2-min',
        'bxslider': 'static/jquery.bxslider',
        'angular-seo': 'static/angular-seo',
        'YTPlayer': 'static/jquery.youtubebackground'
    },
    shim: {
        "angular": {
            exports: "angular"
        },
        'angularroute': {
            deps: ['angular']
        },
        'underscore': {
            deps: ['jquery'],
            exports: '_'
        },
        'device': {
            exports: 'device'
        },
        'parallax': {
            exports: 'parallax'
        },
        'localscroll': {
            exports: 'localscroll'
        },
        'scrollTo': {
            exports: 'scrollTo'
        },
        'bxslider': {
            exports: 'bxslider'
        },
        'angular-seo': {exports: 'seo'},
        'YTPlayer': {
            exports: 'YTPlayer'
        }
    },

    deps: [
        // kick start application... see bootstrap.js
        'jquery',
        'domReady',
        'angularroute',
        'text',
        'underscore',
        'device',
        'parallax',
        'localscroll',
        'scrollTo',
        'bxslider',
        'angular-seo',
        'YTPlayer'
    ]

});

// IE console issue when the developer tools are not opened.
//Ensures there will be no 'console is undefined' errors
if (!window.console) {
    window.console = window.console || (function () {
        var c = {};
        c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () {};
        return c;
    })();
}

/**
 * bootstraps angular onto the window.document node.
 * NOTE: the ng-app attribute should not be on the index.html when using ng.bootstrap
 */
define([
    'require',
    'angular',
    'app/app',
    'app/routes'
], function (require, ng) {

    /*
     * place operations that need to initialize prior to app start here
     * using the `run` function on the top-level module
     */

    require(['domReady!'], function (document) {
        ng.bootstrap(document, ['app']);
    });
});