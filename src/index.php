<?php
  $seourl = 'http://webeffects.herokuapp.com';

  if(isset($_GET['_escaped_fragment_'])){
    $ch = curl_init();

    $url = $seourl . '?_escaped_fragment_=' . $_GET['_escaped_fragment_'];
    
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);

    curl_close($ch);
    echo($output);
  }else{
    echo(file_get_contents('index.html'));
  }
  
?>