/**
 * Grunt config example:
 *  replacer: {
 *    remoteFolder: 'w3/',
 *    localFolder: '<%= config.app %>/images/'
 *  },
 *  replace: {
 *    dist: {
 *      options: {
 *        usePrefix: false,
 *        patterns: ''
 *      },
 *      files: [
 *        {
 *          expand: true,
 *          dest: 'tmp/',
 *          flatten: true,
 *          src: ['tmp/*.css', 'tmp/*.html']
 *        }
 *      ]
 *    }
 *  }
 */

var _ = require('underscore'),
    async = require('async'),
    CloudinaryUploader = require('cloudinary-uploader'),

    replacer = function (grunt, files) {

    var matches = {
        css: function (fileName) {
            return new RegExp('(url\\(["\'])(?:https?:\/\/)?(?:[A-Za-z0-9._-]*/)*' + fileName + '(["\']\\))', 'g');
        },
        html: function (fileName) {
            return new RegExp('(src=["\'])(?:https?:\/\/)?(?:[A-Za-z0-9._-]*/)*' + fileName + '(["\'])', 'g');
        },
        preloaderImg: function (fileName) {
            return new RegExp('(data-image-img=["\'])(?:https?:\/\/)?(?:[A-Za-z0-9._-]*/)*' + fileName + '(["\'])', 'g');
        },
        preloaderBg: function (fileName) {
            return new RegExp('(data-image-bg=["\'])(?:https?:\/\/)?(?:[A-Za-z0-9._-]*/)*' + fileName + '(["\'])', 'g');
        }
    };
    var patterns = {
        css: _.map(files, function (file) {
            return {match: matches.css(file.name), replacement: '$1' + file.url + '$2'}
        }),
        html: _.map(files, function (file) {
            return {match: matches.html(file.name), replacement: '$1' + file.url + '$2'}
        }),
        preloaderImg: _.map(files, function (file) {
            return {match: matches.preloaderImg(file.name), replacement: '$1' + file.url + '$2'}
        }),
        preloaderBg: _.map(files, function (file) {
            return {match: matches.preloaderBg(file.name), replacement: '$1' + file.url + '$2'}
        })
    };

    grunt.config.set('replace.dist.options.patterns', [
        patterns.css,
        patterns.html,
        patterns.preloaderImg,
        patterns.preloaderBg
    ]);
    grunt.task.run('replace:dist')
};
/**
 * Example collection of files:
 * var files = [
 *   {name: 'sprite.png', url: 'some/url/sprite.png'},
 *   {name: 'logo.png', url: 'some/url/logo.png'},
 *   {name: 'add-logo.png', url: 'some/url/add-logo.png'},
 *   {name: 'project-slider-img01.png', url: 'some/url/project-slider-img01.png'},
 *   {name: 'bg-image03.jpg', url: 'some/url/bg-image03.jpg'}
 * ];
 * @param grunt
 */
module.exports = function(grunt) {
    grunt.registerTask('replacer', function() {
        var done = this.async(),
            uploadConfig = grunt.config.get('replacer'),

            cu = new CloudinaryUploader({
                cloud_name: uploadConfig.cloud_name,
                api_key: uploadConfig.api_key,
                api_secret: uploadConfig.api_secret,
                remoteFolder: uploadConfig.remoteFolder,
                localFolder: uploadConfig.localFolder
            });

        async.waterfall([
            function(wfCallback){
                cu.deleteFolder(cu.remoteFolder, wfCallback)
            },
            function(res, wfCallback){
                cu.uploadFiles(null, wfCallback)
            },
            function(uploadedFiles, skippedFiles, wfCallback){
                if (skippedFiles.length){
                    console.log('skipped files:\n', skippedFiles);
                }

                var files = _.map(uploadedFiles, function(descriptor){
                    return {name: descriptor.full_original_name, url: descriptor.url}
                });

                wfCallback(null, files)
            }
        ], function(error, files){
            replacer(grunt, files);
            done();
        });
    });
};