module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Project settings
        config: {
            // Configurable paths
            app: 'src',
            build: 'build'
        },
        // Watches files for changes and runs tasks based on the changed files
        watch: {
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= config.app %>/*.html',
                    '{.tmp,<%= config.app %>}/js/**/*.*',
                    '{.tmp,<%= config.app %>}/partials/**/*.*',
                    '{.tmp,<%= config.app %>}/less/**/*.less', // Add less files to dynamically reload
                    '<%= config.app %>/less/**/*.*',
                    '<%= config.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%= config.app %>/{,*/}*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= config.app %>/images/{,*/}*'
                ]
                //tasks: ['less']
            }
        },
        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    protocol: 'http',
                    open: 'http://localhost:9000/',
                    base: [
                        '.tmp',
                        '<%= config.app %>'
                    ]
                }
            },
            build: {
                options: {
                    protocol: 'http',
                    open: 'http://localhost:9000/',
                    base: '<%= config.build %>'
                }
            }
        },
        clean:{
            tmp:{
                src: ['tmp/*']
            }
        },
        less: {
            compile: {
                files: [{
                    src: 'src/less/main.less',
                    dest: 'src/css/main.css'
                }],
                yuicompress: true
            }
        },
        requirejs: {
            build: {
                // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
                options: {
                    out: './build/js/main.js',
                    name: 'main',
                    mainConfigFile: './src/js/main.js',
                    baseUrl: '<%= config.app %>/js',
                    optimize: 'uglify2',
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: true
                        //uglify2: {} // https://github.com/mishoo/UglifyJS2
                }
            },
            buildCDN: {
                options: {
                    out: './build/js/main.js',
                    name: 'main',
                    mainConfigFile: './tmp/js/main.js',
                    baseUrl: 'tmp/js',
                    optimize: 'uglify2',
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: true
                }
            }
        },
        copy: {
            less: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '.tmp/css',
                    dest: '<%= config.build %>/css',
                    src: [
                        'main.css'
                    ]
                }]
            },
            css: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>/less/lib/',
                    dest: '<%= config.app %>/css',
                    src: [
                        'main.css'
                    ]
                }]
            },
            build: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.build %>',
                    src: [
                        '*.{ico,png,txt}',
                        'images/**/*.*',
                        'video/**/*.*',
                        'css/**/*.*',
                        'index.html',
                        'bower_components/requirejs/require.js',
                        'fonts/*.*',
                        'index.php'
                    ]
                }]
            },
            buildCDN: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'tmp',
                    dest: '<%= config.build %>',
                    src: [
                        '*.{ico,png,txt}',
                        'images/**/*.*',
                        'video/**/*.*',
                        'css/**/*.*',
                        'index.html',
                        'bower_components/requirejs/require.js',
                        'fonts/*.*',
                        'index.php'
                    ]
                }]
            },
            srcCDN: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: 'tmp',
                    src: ['**/*.*']
                }]
            }
        },
        replacer: {
            cloud_name: 'webeffects',
            api_key: '554474958374369',
            api_secret: 'vPd4bZDiJdbJXIkTJknsz0B8ZxI',
            remoteFolder: 'w3/',
            localFolder: '<%= config.app %>/images/'
        },
        replace: {
            dist: {
                options: {
                    usePrefix: false,
                    patterns: ''
                },
                files: [
                    {
                        expand: true,
                        dest: 'tmp/',
                        cwd: 'tmp/',
                        src: ['**/*.css', '**/*.html']
                    }
                ]
            }
        }

    });

    grunt.loadTasks('gruntTasks');

    grunt.registerTask('server', function (target) {
        if (target === 'build') {
            return grunt.task.run(['build', 'connect:build:keepalive']);
        }

        if (target === 'buildCDN') {
            return grunt.task.run(['buildCDN', 'connect:build:keepalive']);
        }

        grunt.task.run([
            'connect:livereload',
            'watch'
        ]);

    });


    grunt.registerTask('build', [
        //'less',
        //'copy:css',
        'requirejs:build',
        'copy:build'
    ]);

    grunt.registerTask('buildCDN', [
        'clean:tmp',
        'copy:srcCDN',
        'replacer',
        'requirejs:buildCDN',
        'copy:buildCDN'
    ]);

    grunt.registerTask('default', [

    ]);

    grunt.registerTask('devserver', [
        //'less',
        //'copy:css',
        'server'
    ]);

    grunt.registerTask('buildserver', [
        'server:build'
    ]);
};